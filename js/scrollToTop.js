const scrollToTop = () => {

    const scrollToTopButton = document.getElementById('scrollToTopButton')

    scrollToTopButton.addEventListener('click', (event) => {
        event.preventDefault()

        seamless.scrollIntoView(document.getElementById("header"), {
            behavior: "smooth",
            block: "center",
            inline: "center",
        })
    })

}

scrollToTop()