const detailsData = () => {

    const preloader = document.getElementById('preloader')

    const renderGenresDropdown = (genres) => {

        const dropdownBlock = document.querySelector('.header__menu .dropdown')

        genres.forEach(genre => {
            dropdownBlock.insertAdjacentHTML('beforeend', `
                <li><a href="./categories.html?genre=${genre}">${genre}</a></li>
            `)
        })

    }

    const renderAnimeDetails = (array, itemId) => {

        const animeObj = array.find(item => item.id == itemId)
        const imageBlock = document.querySelector('.anime__details__pic'),
              viewsBlock = imageBlock.querySelector('.view'),
              titleBlock = document.querySelector('.anime__details__title h3'),
              subtitleBlock = document.querySelector('.anime__details__title span'),
              descriptionBlock = document.querySelector('.anime__details__text p'),
              widgetBlock = document.querySelectorAll('.anime__details__widget ul li')
    
        if (animeObj) {
            imageBlock.dataset.setbg = animeObj.image
            viewsBlock.insertAdjacentHTML('beforeend', `
                <i class="fa fa-eye"></i> ${animeObj.views}
            `)

            titleBlock.textContent = animeObj.title
            subtitleBlock.textContent = animeObj['original-title']
            descriptionBlock.textContent = animeObj.description

            widgetBlock[0].insertAdjacentHTML('beforeend', `
                <span>Date aired:</span> ${animeObj.date}
            `)
            widgetBlock[1].insertAdjacentHTML('beforeend', `
                <span>Rating:</span> ${animeObj.rating}
            `)
            widgetBlock[2].insertAdjacentHTML('beforeend', `
                <span>Genre:</span> ${animeObj.tags.join(', ')}
            `)

            document.querySelectorAll('.set-bg').forEach(elem => {
                elem.style.backgroundImage = `url(${elem.dataset.setbg})`
            })

            setTimeout(() => {
                preloader.classList.remove('active')
            }, 500)
        } else {
            console.log('Anime doesn\'t exist')
        }
    
    }

    fetch('https://animemovieapp-c6d25-default-rtdb.firebaseio.com/anime.json')
    .then((response) => response.json())
    .then((data) => {
        const genres = new Set()
        const genreParams = new URLSearchParams(window.location.search).get('itemId')
        
        data.forEach(item => {
            genres.add(item.genre)
        })

        renderGenresDropdown(genres)
        
        if (genreParams) {
            renderAnimeDetails(data, genreParams)
        } else {
           console.log("Sorry, that anime doesn\'t exist")
        }
        
    })

}

detailsData()