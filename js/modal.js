const modal = () => {

    const modalBtn = document.querySelector('.icon_search'),
          modalBlock = document.querySelector('.search-modal'),
          modalCloseBtn = modalBlock.querySelector('.search-close-switch'),
          modalForm = modalBlock.querySelector('.search-modal-form'),
          modalInput = modalBlock.querySelector('#search-input')

    modalBtn.addEventListener('click', () => {
        modalBlock.style.display = 'block'
    })

    modalCloseBtn.addEventListener('click', () => {
        modalBlock.style.display = 'none'
    })

    // Results
    const renderResults = (array, title) => {
    
        modalBlock.classList.add('results-mode')

        const wrapper = document.querySelector('.search-results .col-lg')
        
        wrapper.innerHTML = ''

        const results = array.filter(item => item.title.toLowerCase().includes(title))
        
        if (!results || results.length === 0) {

            const alertBlock = document.createElement('h4')
            alertBlock.classList.add('results-alert')
            alertBlock.textContent = "Sorry, nothing has been found"

            wrapper.append(alertBlock)

        } else {

            wrapper.innerHTML = ''

            const listBlock = document.createElement('div')
            listBlock.classList.add('row')
            
            results.forEach(result => {
                const tagsBlock = document.createElement('ul')

                result.tags.forEach(tag => {
                    tagsBlock.insertAdjacentHTML('beforeend', `
                        <li>${tag}</li>
                    `)
                })

                listBlock.insertAdjacentHTML('beforeend', `
                    <div class="col-lg-3 col-md-5 col-sm-5">
                        <div class="product__item">
                            <div class="product__item__pic set-bg" data-setbg="${result.image}">
                                <div class="rating">${result.rating} / 10</div>
                                <div class="view"><i class="fa fa-eye"></i> ${result.views}</div>
                            </div>
                            <div class="product__item__text">
                                ${tagsBlock.outerHTML}
                                <h5><a href="./anime-details.html?itemId=${result.id}">${result.title}</a></h5>
                            </div>
                        </div>
                    </div>
                `)
            })

            wrapper.append(listBlock)

            wrapper.querySelectorAll('.set-bg').forEach(elem => {
                elem.style.backgroundImage = `url(${elem.dataset.setbg})`
            })
        }
    
    }

    const searchTitles = (array) => {
    
        modalForm.addEventListener('submit', (event) => {
            event.preventDefault()

            let inputValue = String(modalInput.value.trim().toLowerCase())

            if (inputValue != "") renderResults(array, inputValue) 
        })

    }    

    fetch('https://animemovieapp-c6d25-default-rtdb.firebaseio.com/anime.json')
    .then((response) => response.json())
    .then((data) => {
        searchTitles(data)
    })

}

modal()